﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class moveShip : MonoBehaviour
{

    private Vector2 movement;
    public Vector2 speed;
    // Start is called before the first frame update
    void Start()
{
        movement = new Vector2(2f, 1f);
        speed = new Vector2(4f, 4f);

    }


    // Update is called once per frame
    void Update()
    {
        // Déplacement
 
        float inputY = Input.GetAxis("Vertical");
        float inputX = Input.GetAxis("Horizontal");


        movement = new Vector2(speed.x * inputX, speed.y * inputY);
        GetComponent<Rigidbody2D>().velocity = movement;

        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
       
       
    }
}
