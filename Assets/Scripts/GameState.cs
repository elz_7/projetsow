﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameState : MonoBehaviour
{

    public static GameState Instance = null;


    private int scorePlayer = 0;
    private int lifePlayer = 5;
    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(Instance.gameObject);
        }
        else if (this != Instance)
        {
            Debug.Log("Detruit");
            Destroy(this.gameObject);
        }
    }
    void Update() { 
    
        if(lifePlayer <=0){

        SceneManager.LoadScene("gameOver");
        }

    }

        void FixedUpdate()
    {
        GameObject.FindWithTag("scoreLabel").GetComponent<Text>().text = "" + scorePlayer;
    }

    public void addScorePlayer(int toAdd)
    {
        scorePlayer += toAdd;
    }

    public int getScorePlayer()
    {
        return scorePlayer;
    }

    public void removeLifePlayer()
    {
        lifePlayer -= 1;
    }

}
