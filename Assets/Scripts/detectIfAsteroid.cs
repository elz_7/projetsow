﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class detectIfAsteroid : MonoBehaviour
{
	private GameObject[] respawns;
	private Vector3 siz;

	private Vector3 rightTopCameraBorder;
	private Vector3 rightBottomCameraBorder;
	// Start is called before the first frame update
	void Start()
    {
		rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
		rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));

		
	}

	void Update()
	{
		//	Création	d’un	tableau	contenant tous	les	asteroids	présents dans	la	scene	
		respawns = GameObject.FindGameObjectsWithTag("asteroid");
		//	Si	le	tableau	contient	au	moins	un	élément,	récupération	de	la	taille	d’un	astéroid	(le	premier)	
		if (respawns.Length > 0)
		{
			siz.x = respawns[0].GetComponent<SpriteRenderer>().bounds.size.x;
			siz.y = respawns[0].GetComponent<SpriteRenderer>().bounds.size.y;
		}
		//	Si	le	tableau	contient moins	de	10	éléments
		if (respawns.Length < 10)
		{
			//	Tirage d’une	chance	de	créer ou	pas	un	astéroid
			if (Random.Range(1, 100) == 50 || respawns.Length < 4)
			{
				//	Création nouvel astéroid	avec	une	position	en	Y	aléatoire
				Vector3 tmpPos = new Vector3(rightBottomCameraBorder.x + (siz.x / 2),Random.Range(rightBottomCameraBorder.y + (siz.y / 2),(rightTopCameraBorder.y - (siz.y / 2))),transform.position.z);	
	//	Instanciation	de	l’astéroid	via	le	préfab
				GameObject gY = Instantiate	(Resources.Load	("asteroidSP"),	tmpPos,	Quaternion.identity) as GameObject;
				gY.gameObject.tag = "asteroid";
			}
		}
	}
}
