﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shootAgain : MonoBehaviour
{

	private Vector3 size;
	// Start is called before the first frame update
	void Start()
    {
        
    }

	// Update is called once per frame
	void Update()
	{
		//	Récupération	de	la	taille	de	l’objet auquel est	ajaché	ce	script	(le	vaisseau)	
		size.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
		size.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;
		//	Si	appui sur	la	touche espace
		bool sp = Input.GetKeyDown(KeyCode.Space);
		if (sp)
		{
			//	La	position	de	création	du	tir	se	situe à	la	droite	du	vaisseau
			Vector3 tmpPos = new Vector3(transform.position.x	+	size.x,	transform.position.y,transform.position.z);	
//	Création d’une	instance	d’un	prefab	de	type	shootOrange
			GameObject gY = Instantiate	(Resources.Load	("shootOrange"),tmpPos,	Quaternion.identity, GameObject.FindGameObjectWithTag("foes").transform) as GameObject;
			SoundState.Instance.touchButtonSound();
		}
	}
}
