﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveAsteroid : MonoBehaviour
{
    private Vector2 movement;

    private Vector3 rightTopCameraBorder;
    private Vector3 leftBottomCameraBorder;

    private Vector3 asteroidOldCenterPosition;

    private Vector3 size;

    private bool gameIsShuttingDown = false;
    // Start is called before the first frame update
    void Start()
    {
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));

        size.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        size.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;
        size.z = gameObject.GetComponent<SpriteRenderer>().bounds.size.z;

        this.movement = new Vector2(
            Random.Range(-4.0f, -2f),
            Random.Range(-2.0f, 2.0f)
        );
        GetComponent<Rigidbody2D>().velocity = this.movement;
    }



    void Update()
    {
        asteroidOldCenterPosition = gameObject.GetComponent<Transform>().position;

        if (asteroidOldCenterPosition.x - (size.x / 2) > rightTopCameraBorder.x) { //Debug.Log("destroy"); 
            Destroy(gameObject); }     // L'objet veut sortir à droite 
        if (asteroidOldCenterPosition.x + (size.x / 2) < leftBottomCameraBorder.x) { //Debug.Log("destroy");
            Destroy(gameObject); }       // L'objet veut sortir à gauche
        if (asteroidOldCenterPosition.y - (size.y / 2) > rightTopCameraBorder.y) { //Debug.Log("destroy"); 
            Destroy(gameObject); }       // L'objet veut sortir en haut
        if (asteroidOldCenterPosition.y + (size.y / 2) < leftBottomCameraBorder.y) { //Debug.Log("destroy"); 
            Destroy(gameObject); } // L'objet veut sortir en bas
    }


    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.name =="myShip")
        {
            //Debug.Log(collider.name);

            if (GameObject.FindGameObjectWithTag("life5"))
            {
                GameObject.FindGameObjectWithTag("life5").AddComponent<fadeOut>();
            }
            else if (GameObject.FindGameObjectWithTag("life4"))
            {
                GameObject.FindGameObjectWithTag("life4").AddComponent<fadeOut>();
            }
            else if (GameObject.FindGameObjectWithTag("life3"))
            {
                GameObject.FindGameObjectWithTag("life3").AddComponent<fadeOut>();
            }
            else if (GameObject.FindGameObjectWithTag("life2"))
            {
                GameObject.FindGameObjectWithTag("life2").AddComponent<fadeOut>();
            }
            else if (GameObject.FindGameObjectWithTag("life1"))
            {
                GameObject.FindGameObjectWithTag("life1").AddComponent<fadeOut>();
            }

            GameState.Instance.removeLifePlayer();
        }
    }

    void OnApplicationQuit()
    {
        this.gameIsShuttingDown = true;
    }

    void OnDestroy()
    {

        if(this.gameIsShuttingDown == false){ // evite les risidu apres avoir fermer la scene
            GameObject gY = Instantiate(Resources.Load("asteroidSP"), new Vector3(rightTopCameraBorder.x, asteroidOldCenterPosition.y, 0), Quaternion.identity,GameObject.FindGameObjectWithTag("foes").transform) as GameObject; // instencie un clone à droite de l'écran qui garde la même position Y que l'asteroid dedruit.
            gY.gameObject.tag = "asteroid";
        }
        
    }
}
