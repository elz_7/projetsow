﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class posShip : MonoBehaviour {

    private Vector3 cameraBorderDotRightTop;
    private Vector3 cameraBorderDotRightBottom;
    private Vector3 cameraBorderDotLeftBottom;
    private Vector3 cameraBorderDotLeftTop;
    private Vector3 shipSize;
    private Vector3 currentPos;
    private Vector3 shipOldCenterPosition;
    private Vector2 shipNewCenterPosition;

    // Start is called before the first frame update
    void Start() {
        cameraBorderDotLeftBottom = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        cameraBorderDotRightBottom = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        cameraBorderDotLeftTop = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        cameraBorderDotRightTop = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));

        shipSize = gameObject.GetComponent<SpriteRenderer>().bounds.size;       // Récupération de la taille de l'objet de "ship".
        currentPos = gameObject.transform.position;

        gameObject.transform.position = new Vector3(cameraBorderDotLeftTop.x, currentPos.y, currentPos.z);
    
    
  
    }

    // Update is called once per frame
    void Update() {
        
        shipOldCenterPosition = gameObject.GetComponent<Transform>().position;  // Récupération de la position de l'objet.

        shipNewCenterPosition = shipOldCenterPosition;  // Valeur par défaut (cas où l'objet est dans le champs de vision de la caméra).

        if (shipOldCenterPosition.x + (shipSize.x/2) > cameraBorderDotRightTop.x) { shipNewCenterPosition.x = cameraBorderDotRightTop.x - (shipSize.x/2); }     // L'objet veut sortir à droite 
        if (shipOldCenterPosition.x - (shipSize.x/2) < cameraBorderDotLeftTop.x) { shipNewCenterPosition.x = cameraBorderDotLeftTop.x + (shipSize.x/2); }       // L'objet veut sortir à gauche
        if (shipOldCenterPosition.y + (shipSize.y/2) > cameraBorderDotLeftTop.y) { shipNewCenterPosition.y = cameraBorderDotLeftTop.y - (shipSize.y/2); }       // L'objet veut sortir en haut
        if (shipOldCenterPosition.y - (shipSize.y/2) < cameraBorderDotLeftBottom.y) { shipNewCenterPosition.y = cameraBorderDotLeftBottom.y + (shipSize.y/2); } // L'objet veut sortir en bas

        gameObject.GetComponent<Rigidbody2D>().position = shipNewCenterPosition;
    }
}
