﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveShoot : MonoBehaviour
{

    public Vector2 movement;

    private Vector3 rightTopCameraBorder;
    private Vector3 leftBottomCameraBorder;

    private Vector3 shootOldCenterPosition;

    private Vector3 size;

    private float  moveHorizontal;
    
    private bool gameIsShuttingDown = false;

    // Start is called before the first frame update
    void Start()
    {
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));

        size.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        size.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;
        size.z = gameObject.GetComponent<SpriteRenderer>().bounds.size.z;

        moveHorizontal = Input.GetAxis("Horizontal");
        this.movement = new Vector2(4, moveHorizontal);
        GetComponent<Rigidbody2D>().velocity = this.movement;
    }

    // Update is called once per frame
    void Update()
    {
        shootOldCenterPosition = gameObject.GetComponent<Transform>().position;

        if (shootOldCenterPosition.x - (size.x / 2) > rightTopCameraBorder.x)
        { //Debug.Log("destroy"); 
            Destroy(gameObject);
        }     // L'objet veut sortir à droite 


        if (shootOldCenterPosition.x + (size.x / 2) < leftBottomCameraBorder.x)
        { //Debug.Log("destroy");
            Destroy(gameObject);
        }       // L'objet veut sortir à gauche


        if (shootOldCenterPosition.y - (size.y / 2) > rightTopCameraBorder.y)
        { //Debug.Log("destroy"); 
            Destroy(gameObject);
        }       // L'objet veut sortir en haut

        if (shootOldCenterPosition.y + (size.y / 2) < leftBottomCameraBorder.y)
        { //Debug.Log("destroy"); 
            Destroy(gameObject);
        } // L'objet veut sortir en bas

    }

    void OnApplicationQuit()
    {
        this.gameIsShuttingDown = true;
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
       

        collider.gameObject.AddComponent<fadeOut>();
        
        Destroy(gameObject);
        GameState.Instance.addScorePlayer(1);
    }
}
