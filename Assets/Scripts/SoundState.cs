﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundState : MonoBehaviour
{

    public static SoundState Instance = null;
    public AudioClip playerShotSound;
    
    // Start is called before the first frame update
    void Start()
    {

        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(Instance.gameObject);
        }
       

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void touchButtonSound()
    {
        MakeSound(playerShotSound);
    }

    ///	Lance	la	lecture	d'un	son
    private void MakeSound(AudioClip originalClip)
    {
        AudioSource.PlayClipAtPoint(originalClip, transform.position);	

                }
}
